//
//  RoundedCollectionViewCell.swift
//  Kolesa
//
//  Created by Make on 10/14/19.
//  Copyright © 2019 Make. All rights reserved.
//

import UIKit

class RoundedCollectionViewCell: UICollectionViewCell {
    
    override func layoutSubviews() {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 10
        self.layer.shadowRadius = 3
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 10, height: 10)
    }
}


