//
//  CategoryCell.swift
//  Kolesa
//
//  Created by Make on 10/8/19.
//  Copyright © 2019 Make. All rights reserved.
//

import UIKit

class CategoryCell: RoundedCollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    func setData(category: Category) {
        titleLabel.text = category.title
        imageView.image = UIImage(named: "preloader")
        NetworkService.shared.getJSON(urlPath: category.image) { data in
            self.imageView.image = UIImage(data: data)
        }
    }
}
