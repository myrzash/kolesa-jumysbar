//
//  CategoriesProvider.swift
//  Kolesa
//
//  Created by Make on 10/9/19.
//  Copyright © 2019 Make. All rights reserved.
//

import Foundation

class CategoryProvider {
    private init () {}
    
    static func fetchCategories (_ completion: @escaping([Category]) -> ()) {
        NetworkService.shared.getData(urlPath: "https://lomiren.kz/intern/category") {completion($0)}
    }
}
