//
//  Category.swift
//  Kolesa
//
//  Created by Make on 10/8/19.
//  Copyright © 2019 Make. All rights reserved.
//

import Foundation

class Category: Decodable {
    var id: String = ""
    var title: String = ""
    var image: String = ""
}
