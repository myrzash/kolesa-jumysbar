//
//  CollectionViewController.swift
//  Kolesa
//
//  Created by Make on 10/8/19.
//  Copyright © 2019 Make. All rights reserved.
//

import UIKit

private let reuseIdentifier = "CategoryCell"

class CategoriesCollectionViewController: GridViewController{
    
    private var categories = [Category]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CategoryProvider.fetchCategories{ categories in
            self.categories = categories
            self.collectionView.reloadData()
        }
    }
    
    // MARK: UICollectionViewDataSource
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? CategoryCell else {return UICollectionViewCell()}
        
        let category = categories[indexPath.row]
        cell.setData(category: category)
        cell.contentView.frame.size = cell.frame.size
        return cell
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "categoriesToAdverts", let cell = sender as? CategoryCell {
            
            let indexPath = collectionView.indexPath(for: cell)
            let category = categories[indexPath!.item]
            let advertsVC = segue.destination as? AdvertsTableViewController
            advertsVC?.currentCategory = category
            
        }
    }
}
