//
//  GridViewController.swift
//  Kolesa
//
//  Created by Make on 10/14/19.
//  Copyright © 2019 Make. All rights reserved.
//

import UIKit

class GridViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout  {
 
    var estimatedCellWidth = 150.0
    var cellMargin: CGFloat = 5.0
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let insetSection = cellMargin*2
        return UIEdgeInsets(top: insetSection, left: insetSection, bottom: insetSection, right: insetSection)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = calculateCellWidth()
        return CGSize(width: size, height: size)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return cellMargin
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return cellMargin*2
    }
    
    func calculateCellWidth() -> CGFloat {
        let estimateWidth = CGFloat(estimatedCellWidth)
        let cellCount = floor(CGFloat(view.frame.size.width) / estimateWidth)
        
        let totalWidth = (view.frame.size.width - cellMargin * (cellCount - 1) - cellMargin - 4 * cellMargin)
        
        let width = totalWidth / cellCount
        return width
    }
}
