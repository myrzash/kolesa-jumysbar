//
//  DataResponse.swift
//  Kolesa
//
//  Created by Kozhakhmet Myrzagali on 10/15/19.
//  Copyright © 2019 Make. All rights reserved.
//

import Foundation

class DataResponse<T:Decodable>: Decodable {
    
    var data: T
    
    enum CodingKeys: String, CodingKey {
        case data
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.data = try container.decode(T.self, forKey: .data)
    }
}

