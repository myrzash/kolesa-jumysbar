//
//  NetworkService.swift
//  Kolesa
//
//  Created by Make on 10/14/19.
//  Copyright © 2019 Make. All rights reserved.
//

import Foundation

class NetworkService {
    private init () {}
    
    static let shared = NetworkService()
    
    func getData<T:Decodable>(urlPath: String, completion: @escaping(T) -> ()) {
        
        getJSON(urlPath: urlPath){ response in
            do {
                let json = try JSONDecoder().decode(DataResponse<T>.self, from: response)
                completion(json.data)
            } catch {
                print(error)
            }
        }
    }
    
    func getJSON(urlPath: String, completion: @escaping(Data) -> ()){
        
        guard let url = URL(string: urlPath) else {return}
        
        let session = URLSession.shared
        session.dataTask(with: url) { (data, response, error) in
            
            if let data = data {
                DispatchQueue.main.async {
                    completion(data)
                }
            }
            
            }.resume()
    }
}
