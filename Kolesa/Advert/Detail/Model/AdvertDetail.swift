//
//  AdvertDetail.swift
//  Kolesa
//
//  Created by Make on 10/15/19.
//  Copyright © 2019 Make. All rights reserved.
//

import Foundation

class AdvertDetail: Decodable {
    var id: String = ""
    var title: String = ""
    var image: String = ""
    var description: String = ""
    var gallery: [String] = []
    var params: [[String: String]] = [[:]]
}
