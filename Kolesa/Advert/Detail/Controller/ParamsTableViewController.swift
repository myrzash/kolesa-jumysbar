//
//  ParamsTableViewController.swift
//  Kolesa
//
//  Created by Make on 10/15/19.
//  Copyright © 2019 Make. All rights reserved.
//

import UIKit

class ParamsTableViewController: UITableViewController {
    
    private let reuseIdentifier = "ParamsCell"
    private var params: [[String: String]]?
    
    // MARK: - Table view data source
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorColor = UIColor.clear
//        tableView.frame.size = CGSize(width: tableView.frame.width, height: tableView.contentSize.height)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return params?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? ParamsCell,
            let params = params else {
                return UITableViewCell()
        }
        
        let dict = Array(params[indexPath.row])[0]
        cell.setData(dict: dict)
        return cell
    }
    
    func updateTableView(params: [[String: String]]){
        self.params = params
        tableView.reloadData()
    }
    
}
