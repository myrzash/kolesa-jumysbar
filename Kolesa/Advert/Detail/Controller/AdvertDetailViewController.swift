//
//  AdvertDetailViewController.swift
//  Kolesa
//
//  Created by Make on 10/11/19.
//  Copyright © 2019 Make. All rights reserved.
//

import UIKit

class AdvertDetailViewController: UIViewController {
    
    var currentAdvertId: String? // = "79"
    private var galleryVC: GalleryCollectionViewController?
    private var paramsVC: ParamsTableViewController?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var galleryViewController: GalleryCollectionViewController!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let currentAdvertId = self.currentAdvertId else { return }
        AdvertProvider.fetchAdvert(id: currentAdvertId) { advert in
            self.titleLabel.text = advert.title
            self.descriptionTextView.text = advert.description
            
            self.galleryVC?.updateCollectionView(images: advert.gallery)
            self.paramsVC?.updateTableView(params: advert.params)
        }
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        self.galleryVC?.collectionView.reloadData()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "embedGallerySegue",
            let galleryVC = segue.destination as? GalleryCollectionViewController {
            self.galleryVC = galleryVC
        }
        
        if segue.identifier == "embedParamsSegue",
            let paramsVC = segue.destination as? ParamsTableViewController {
            self.paramsVC = paramsVC
        }
    }
}
