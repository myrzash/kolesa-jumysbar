//
//  GalleryCell.swift
//  Kolesa
//
//  Created by Make on 10/15/19.
//  Copyright © 2019 Make. All rights reserved.
//

import UIKit

class GalleryCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    func setImage(_ urlPath: String) {
        imageView.image = UIImage(named: "preloader")
        NetworkService.shared.getJSON(urlPath: urlPath) { data in
            self.imageView.image = UIImage(data: data)
        }
    }}
