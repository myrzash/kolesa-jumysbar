//
//  ParamsTableViewCell.swift
//  Kolesa
//
//  Created by Make on 10/15/19.
//  Copyright © 2019 Make. All rights reserved.
//

import UIKit

class ParamsCell: UITableViewCell {

    @IBOutlet weak var keyLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    func setData(dict: (String, String)){
        keyLabel.text =  dict.0
        valueLabel.text = dict.1
    }
}
