//
//  AdvertsTableViewController.swift
//  Kolesa
//
//  Created by Make on 10/10/19.
//  Copyright © 2019 Make. All rights reserved.
//

import UIKit

class AdvertsTableViewController: UITableViewController {
    
    private let reuseIdentifier = "AdvertCell"
    var currentCategory: Category?
    private var adverts = [Advert]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let currentCategory = self.currentCategory else {return}
        
        self.title = currentCategory.title
        
        AdvertProvider.fetchAdverts(categoryId: currentCategory.id){ adverts in
            self.adverts = adverts
            self.tableView.reloadData()
        }    
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Чтобы растянуть содержимое ячейки
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return adverts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? AdvertCell else {return UITableViewCell()}
        
        let advert = adverts[indexPath.row]
        cell.setData(advert: advert)
        return cell
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "advertsToDetail", let cell = sender as? AdvertCell {
            
            let indexPath = tableView.indexPath(for: cell)
            let advert = adverts[indexPath!.item]
            
            let advertDetailVC = segue.destination as? AdvertDetailViewController
            advertDetailVC?.currentAdvertId = advert.id
            
        }
    }
    
}
