//
//  AdvertCell.swift
//  Kolesa
//
//  Created by Make on 10/10/19.
//  Copyright © 2019 Make. All rights reserved.
//

import UIKit

class AdvertCell: UITableViewCell {
    
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UITextView!
    
    func setData(advert: Advert) {
        titleLabel.text = advert.title
        descriptionLabel.text = advert.description
        coverImageView.image = UIImage(named: "preloader")
        NetworkService.shared.getJSON(urlPath: advert.image) { data in
            self.coverImageView.image = UIImage(data: data)
        }
    }
}
