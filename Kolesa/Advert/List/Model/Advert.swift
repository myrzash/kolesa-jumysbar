//
//  Advert.swift
//  Kolesa
//
//  Created by Make on 10/10/19.
//  Copyright © 2019 Make. All rights reserved.
//

import Foundation

class Advert: Decodable {
    var id: String = ""
    var title: String = ""
    var image: String = ""
    var description: String = ""    
}
