//
//  AdvertProvider.swift
//  Kolesa
//
//  Created by Make on 10/10/19.
//  Copyright © 2019 Make. All rights reserved.
//

import Foundation

class AdvertProvider {
    private init () {}
    
    static func fetchAdverts (categoryId: String, completion: @escaping([Advert]) -> ()) {
        let path = "https://lomiren.kz/intern/category_adverts/\(categoryId)"
        NetworkService.shared.getData(urlPath: path) { completion($0)}
    }
    
    static func fetchAdvert (id: String, completion: @escaping(AdvertDetail) -> ()) {
        let path = "https://lomiren.kz/intern/advert/\(id)"
        NetworkService.shared.getData(urlPath: path) { completion($0)}
    }
}
